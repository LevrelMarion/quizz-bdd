package com.zenika.academy.quiz.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class RandomGeneratorConfiguration {

    @Bean
    public Random random() {
        return new Random();
    }
}
