package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.OpenQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.domain.questions.TrueFalseQuestion;
import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class QuestionRepository {

    public JdbcTemplate template;
    Random random = new Random();

    public QuestionRepository(JdbcTemplate template) {
        this.template = template;
    }

    public List<Question> getAll() {
        final RowMapper<Question> questionRowMapper = (rs, rowNum) ->
        {
            List<String> suggestions = new ArrayList<>();
            suggestions.add(rs.getString("suggestion_1"));
            suggestions.add(rs.getString("suggestion_2"));
            suggestions.add(rs.getString("suggestion_3"));

            if (rs.getString("suggestion_1") != null) {
                return new MultipleChoiceQuestion(rs.getString("id"), rs.getString("question_text"),
                        suggestions, rs.getString("correct_answer_text"), random);
            } else if (rs.getString("correct_answer_bool") != null) {
                return new TrueFalseQuestion(rs.getString("id"), rs.getString("question_text"),
                        rs.getBoolean("correct_answer_bool"));
            } else {
                return new OpenQuestion(rs.getString("id"), rs.getString("question_text"),
                        rs.getString("correct_answer_text"));
            }
        };
        return List.copyOf(this.template.query("SELECT * FROM questions", questionRowMapper));
    }

    public void save(Question q) {
        if (q instanceof OpenQuestion) {
            OpenQuestion qO = (OpenQuestion) q;
            this.template.update("insert into questions (id, question_text, correct_answer_text) values ( ?, ?, ?)",
                    qO.getId(), qO.getDisplayableText(), qO.getCorrectAnswer());
        } else if
        (q instanceof TrueFalseQuestion) {
            TrueFalseQuestion qTF = (TrueFalseQuestion) q;
            this.template.update("insert into questions (id, question_text, correct_answer_bool) values (?, ?, ?)",
                    qTF.getId(), qTF.getDisplayableText(), qTF.getAnswerIsTrue());
        } else if
        (q instanceof MultipleChoiceQuestion) {
            MultipleChoiceQuestion qM = (MultipleChoiceQuestion) q;
            this.template.update("insert into questions (id, question_text, correct_answer_text, suggestion_1, suggestion_2, suggestion_3) values (?, ?, ?, ?, ?, ?)",
                    qM.getId(), qM.getText(), qM.getCorrectAnswerMC(), qM.getSuggestions().get(0),
                    qM.getSuggestions().get(1), qM.getSuggestions().get(2));
        }
    }

    public Optional<Question> getOne(String id) {
        final RowMapper<Question> questionRowMapperId = (rs, rowNum) ->
        {
            List<String> suggestions = new ArrayList<>();
            suggestions.add(rs.getString("suggestion_1"));
            suggestions.add(rs.getString("suggestion_2"));
            suggestions.add(rs.getString("suggestion_3"));

            if (rs.getString("suggestion_1") != null) {
                return new MultipleChoiceQuestion(id, rs.getString("question_text"),
                        suggestions, rs.getString("correct_answer_text"), random);
            } else if (rs.getString("correct_answer_bool") != null) {
                return new TrueFalseQuestion(id, rs.getString("question_text"),
                        rs.getBoolean("correct_answer_bool"));
            } else {
                return new OpenQuestion(id, rs.getString("question_text"),
                        rs.getString("correct_answer_text"));
            }
        };
        return Optional.ofNullable(template.queryForObject("select * from questions where id = ?",
                questionRowMapperId, id));
    }
  /*  final RowMapper<Question> openQuestionRowMapper = (rs, rowNum) ->
            new OpenQuestion(id, rs.getString("question_text"), rs.getString("correct_answer_text"));
    */

  public List<Question> getByQuizId(String quizId) {
        return this.getAll().stream()
                .filter(question -> Objects.equals(question.getQuizId(), quizId))
                .collect(Collectors.toList());
    }

}


// private Map<Long, Question> questionsById = new HashMap<>();
// public void save(Question q) {this.questionsById.put(q.getId(), q); }
// INSERT INTO public.questions VALUE newOpenQuestion(4, enonce, answertext, answerbool, suggestion, suggestion, suggestion)
// public Optional<Question> getOne(long id) {return Optional.ofNullable(this.questionsById.get(id));}
// SELECT * FROM questions WHERE questions.id = 3
    /* public List<Question> getAll() {
        return List.copyOf(this.questionsById.values());
    }*/
// SELECT * FROM questions