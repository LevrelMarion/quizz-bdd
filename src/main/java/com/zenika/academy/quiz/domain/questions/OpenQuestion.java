package com.zenika.academy.quiz.domain.questions;

import org.apache.commons.text.similarity.LevenshteinDistance;

public class OpenQuestion extends AbstractQuestion {

    private final String text;
    private final String correctAnswer;
    private final LevenshteinDistance d;

    public OpenQuestion(String id, String text, String correctAnswer) {
        super(id);

        this.text = text;
        this.correctAnswer = correctAnswer;
        this.d = new LevenshteinDistance();
    }

    /**
     * Returns the text of the question as given in the constructor.
     */
    @Override
    public String getDisplayableText() {
        return this.text;
    }

    /**
     * Try an answer.
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one (case insensitive), ALMOST_CORRECT if the levenshtein
     * distance between the given answer and the correct answer is lower than 2, incorrect otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        final Integer distanceWithCorrectAnswer = this.d.apply(userAnswer.toLowerCase(), this.correctAnswer.toLowerCase());
        if(distanceWithCorrectAnswer == 0) {
            return AnswerResult.CORRECT;
        }
        else if (distanceWithCorrectAnswer < 2) {
            return AnswerResult.ALMOST_CORRECT;
        }
        else {
            return AnswerResult.INCORRECT;
        }
    }

    public String getCorrectAnswer() {
        return this.correctAnswer;
    }

}
