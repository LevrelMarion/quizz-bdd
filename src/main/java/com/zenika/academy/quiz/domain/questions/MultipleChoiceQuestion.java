package com.zenika.academy.quiz.domain.questions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.lang.System.lineSeparator;

public class MultipleChoiceQuestion extends AbstractQuestion {
    private final String text;
    private final List<String> suggestions;
    private final Integer correctAnswerIndex;
    private final String correctAnswer;

    public MultipleChoiceQuestion(String id, String text, List<String> incorrectSuggestions, String correctAnswer, Random random) {
        super(id);
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.suggestions = new ArrayList<>(incorrectSuggestions);
        this.suggestions.add(correctAnswer);
        Collections.shuffle(this.suggestions, random);

        this.correctAnswerIndex = this.suggestions.indexOf(correctAnswer);
    }

    /**
     * The text of the question, followed by the suggestions in a randomized order. Each suggestion also
     * has an index.
     */
    @Override
    public String getDisplayableText() {
        StringBuilder sb = new StringBuilder(this.text + lineSeparator());
        for (int i = 0; i < this.suggestions.size(); i++) {
            String suggestion = this.suggestions.get(i);
            sb.append("  ").append(i+1).append(") ").append(suggestion).append(lineSeparator());
        }
        return sb.toString();
    }


    /**
     * Try an answer.
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one (case insensitive), or if the answer is the number
     * of the right one; INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                this.userAnswerIsCorrectIndex(userAnswer) || this.userAnswerIsCorrectAnswer(userAnswer)
        );
    }

    public String getText() {
        return this.text;
    }

    public List<String> getSuggestions() {
        return this.suggestions;
    }

    private boolean userAnswerIsCorrectAnswer(String userAnswer) {
        return userAnswer.toLowerCase().equals(this.suggestions.get(this.correctAnswerIndex).toLowerCase());
    }

    private boolean userAnswerIsCorrectIndex(String userAnswer) {
        return String.valueOf(this.correctAnswerIndex+1).equals(userAnswer);
    }

    public String getCorrectAnswerMC() {
        return correctAnswer;
    }
}
