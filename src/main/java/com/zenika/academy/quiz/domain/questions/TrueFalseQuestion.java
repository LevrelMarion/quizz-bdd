package com.zenika.academy.quiz.domain.questions;

public class TrueFalseQuestion extends AbstractQuestion {

    private String text;
    private boolean isTrue;

    public TrueFalseQuestion(String id, String text, boolean isTrue) {
        super(id);
        this.text = text;
        this.isTrue = isTrue;
    }

    /**
     * The text of the question, preceded by the string "VRAI ou FAUX ?"
     */
    @Override
    public String getDisplayableText() {
        return this.text;
    }

    /**
     * Try an answer.
     *
     * If the right answer is true, accepted answers are "true", "oui", "vrai".
     * If the right answer is false, accepted answers are "false", "non", "faux".
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                (this.userAnswersYes(userAnswer) && this.isTrue) || this.userAnswersNo(userAnswer) && !this.isTrue
        );
    }

    private boolean userAnswersNo(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("false") || normalizedAnser.equals("non") || normalizedAnser.equals("faux");
    }

    private boolean userAnswersYes(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("true") || normalizedAnser.equals("oui") || normalizedAnser.equals("vrai");
    }

    public boolean getAnswerIsTrue() {
        return isTrue;
    }
}
